<?php
namespace App\Controller;
use App\Entity\UploadForm;
use App\Entity\CloudFile;
use App\Form\UploadFormType;
use App\Controller\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\DependencyInjection\Argument\ServiceLocator;
use  App\Controller\DosyaController;	
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
class DosyaController extends AbstractController
{
    /**
    * @Route("/dosya", name="dosya")
    * 
    */
    public function dosya(Request $request)
    {

       $uploadform = new UploadForm();
        $form = $this->createForm(UploadFormType::class, $uploadform);
        $form->handleRequest($request);
       

      //  var_dump($form->handleRequest($request));die;
        if ($request->isMethod('POST') && $form->isSubmitted() && $form->isValid() ) {
              
            $client = new S3Client(array(
               'bucket' =>$this->getParameter('AWS_S3_BUCKET_NAME'),
                'version' => $this->getParameter('AWS_S3_VERSION'),
                'signature'=>  $this->getParameter('AWS_S3_SIGNATURE'),
                'region'  => $this->getParameter('AWS_S3_REGION'),
                'credentials' => array(
                'key'=> $this->getParameter('AWS_S3_KEY'),
                'secret'=>$this->getParameter('AWS_S3_SECRET')
              ),
               'aws:SecureTransport'=>true,
             
       ));
       
             $files=$request->files->all();
             
             $file=$files['upload_form']['uploadfile'];
               
            /**@var UploadedFile $uploadFile */
            $uploadFile = $form['uploadfile']->getData();
            $originalFilename = pathinfo($uploadFile->getClientOriginalName(), PATHINFO_FILENAME);
            $newFilename=$file->getClientOriginalName();
            $uploadfile = $newFilename;
             
             $name=$file->getClientOriginalName();
             
             $fileSize= $file->getClientSize();
             
             $file->move("/home/bahar/Documents/file/",$uploadfile);

             $result =$client->putObject(array(
             'Bucket' =>$this->getParameter('AWS_S3_BUCKET_NAME'),
             'Key'    => $name,
             'Body'   => fopen('/home/bahar/Documents/file/'.$uploadfile,'rb'),
             'ACL'   => "",
             
             ));  
             $register =new cloudFile();

             $now = new\DateTime('now');
           
             $register->setUploadTime($now);
             $register->setFileSize($fileSize);
             $register->setPath($uploadfile);
             $em = $this-> getDoctrine()->getManager();
             $em -> persist($register);
             $em ->flush();
            

            }
           
  

            $liste = $this-> getDoctrine()->getManager();
            $list= $liste->getRepository(CloudFile::class)->findBy(array(),array('uploadTime' => 'DESC')); 

          return $this->render('dosya/bulutron.html.twig', [
            'upload_form' => $form->createView(),
            'list' => $list,
        ]);
        }
        

 /**
   * @Route("/delete/{id}", name="delete")
   */
  public function delete($id){
    
    $client = new S3Client(array(
      'bucket' =>$this->getParameter('AWS_S3_BUCKET_NAME'),
      'version' => $this->getParameter('AWS_S3_VERSION'),
      'signature'=>  $this->getParameter('AWS_S3_SIGNATURE'),
      'region'  => $this->getParameter('AWS_S3_REGION'),
      'credentials' => array(
      'key'=> $this->getParameter('AWS_S3_KEY'),
      'secret'=>$this->getParameter('AWS_S3_SECRET')
    ),
     'aws:SecureTransport'=>true,
   
  ));
  //veritabanından dosya buluyor.
  $db = $this-> getDoctrine()->getManager();
  $list = $db-> getRepository(CloudFile::class)->find($id);
  $fileName = $list->getPath();

    $client->deleteObject([
        'Bucket' =>$this->getParameter('AWS_S3_BUCKET_NAME'),
        'Key'    =>  $fileName
    ]); 

     $db = $this-> getDoctrine()->getManager();
    $list = $db-> getRepository(CloudFile::class)->find($id);
    $db -> remove($list);  
    $db -> flush();

return $this->redirect('/dosya');
  }

/**
* @Route("/download/{link}", name="download")
*/

public function download($link){  
$client = new S3Client(array(
  'bucket' =>$this->getParameter('AWS_S3_BUCKET_NAME'),
  'version' => $this->getParameter('AWS_S3_VERSION'),
  'signature'=>  $this->getParameter('AWS_S3_SIGNATURE'),
  'region'  => $this->getParameter('AWS_S3_REGION'),
  'credentials' => array(
  'key'=> $this->getParameter('AWS_S3_KEY'),
  'secret'=>$this->getParameter('AWS_S3_SECRET')
),
'aws:SecureTransport'=>true,
));

$result= $client->getCommand('GetObject', [
'Bucket' =>$this->getParameter('AWS_S3_BUCKET_NAME'),
'Key'    => $link,
'credentials' => array(
'key' => $this ->getParameter('AWS_S3_KEY'),
'secret'=>$this->getParameter('AWS_S3_SECRET'),
),
'aws:SecureTransport'=>true,
]);


$request = $client->createPresignedRequest($result, '+1 minute');
$presignedUrl = (string) $request->getUri();

return $this->redirect($presignedUrl);



}


 
}