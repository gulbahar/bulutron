<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AnasayfaController extends AbstractController
{
    /**
     * @Route("/anasayfa", name="anasayfa")
     */
    public function index()
    {
        return $this->render('anasayfa/index.html.twig', [
            'controller_name' => 'AnasayfaController',
        ]);
    }
}
