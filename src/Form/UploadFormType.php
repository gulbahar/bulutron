<?php 
namespace App\Form;


use App\Entity\UploadForm;

use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;




class UploadFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
    
        ->add('uploadfile', FileType::class, [

      'attr' =>[
                'style' => 'margin-bottom:10px' 
                ],
            // unmapped means that this field is not associated to any entity property
            'mapped' => false,

            // everytime you edit the Product details
            'required' => true,

            // unmapped fields can't define their validation using annotations
            // in the associated entity, so you can use the PHP constraint classes
            'constraints' => [
                new File([
                    'maxSize' => '1024k',
                    'mimeTypes' => [
                        'application/pdf',
                        'application/x-pdf',
                        'image/*',
                        'application/msword',
                        'application/vnd.ms-excel',
                        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                        'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                        'text/plain'
                ],
                    'mimeTypesMessage' => 'Lütfen Dosya Yükleyin!',
                ])
            ],
        ]) 
         

     
        // ...    
        /*
 
        ->add('Yükle', SubmitType::class,[
        'attr' =>[
            'class' => 'btn btn-primary',
            'style' => 'margin-left:300px' 

             ]  ])*/

           ;






    }
           
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
           'data_class' =>UploadForm::class,
        ]);
    }
}