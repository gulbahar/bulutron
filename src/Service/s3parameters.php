<?php 
namespace App\Service;
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;


class s3parameters
{
    public function s3paramet()
    {
        $client = [
           
           S3Client(array(
                'bucket' =>'bulutrons3',
                 'version' => '%env(AWS_S3_VERSION)%',
                 'signature'=> '%env(AWS_S3_SIGNATURE)%' ,
                 'region'  => '%env(AWS_S3_REGION)%',
                 'credentials' => array(
                 'key'=> '%env(AWS_S3_KEY)%',
               'secret'=> '%env(AWS_S3_SECRET)%'
                 
               ),
                'aws:SecureTransport'=>true,
              
        ))
              
        
        ];

        $index = array_rand($client);

        return $client[$index];
    }
}