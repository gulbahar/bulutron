<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UploadFormRepository")
 */
class UploadForm
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    //private $id;
     /**
     * @ORM\Column(type="string",length=255)
     */
     /**
     * @ORM\Column(type="text")
     *//*
   public function getId():?int
    {
        return $this->id;
    }*/
      /**
     * @ORM\Column(type="string")
     */
    private $uploadFilename;

    public function getuploadFilename()
    {
        return $this->uploadFilename;
    }

    public function setuploadFilename($uploadFilename)
    {
        $this->uploadFilename = $uploadFilename;

        return $this;
    }
    


}
