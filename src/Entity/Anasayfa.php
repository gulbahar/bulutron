<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AnasayfaRepository")
 */
class Anasayfa
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
     /**
     * @ORM\Column(type="string",length=255)
     */
     /**
     * @ORM\Column(type="text")
     */
    public function getId():?int
    {
           return $this->id;
   }
   public function setId($id)
   {
       $this->id = $id;

       return $this;
   }}