<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CloudFileRepository")
 */
class CloudFile
{ /**
    * @ORM\Id()
    * @ORM\GeneratedValue()
    * @ORM\Column(type="integer")
    */
   private $id;
    /**
    * @ORM\Column(type="string",length=255)
    */
    /**
    * @ORM\Column(type="text")
    */
   public function getId():?int
   {
          return $this->id;
  }
  public function setId($id)
  {
      $this->id = $id;

      return $this;
  }


  
    /** 
     *@var \DateTime
     *
     * @ORM\Column(name="upload_time", type="datetime")
     */
    private $uploadTime;

    /**
     *@var string
     *
     * @ORM\Column(name="file_size", type="string", length=255)
     */
    private $fileSize;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     */
    private $path;

  


   
/**
     * Set uploadTime
     *
     * @param \DateTime $uploadTime
     *
     * @return cloudFile
     */
    public function setUploadTime($uploadTime)
    {
        $this->uploadTime = $uploadTime;

        return $this;
    }

    /**
     * Get uploadTime
     *
     * @return \DateTime
     */
    public function getUploadTime()
    {
        return $this->uploadTime;
    }

    /**
     * Set fileSize
     *
     * @param string $fileSize
     *
     * @return cloudFile
     */
    public function setFileSize($fileSize)
    {
        $this->fileSize = $fileSize;

        return $this;
    }

    /**
     * Get fileSize
     *
     * @return string
     */
    public function getFileSize()
    {
        return $this->fileSize;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return cloudFile
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }



    
}



