<?php

namespace App\Repository;

use App\Entity\Dosya;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Dosya|null find($id, $lockMode = null, $lockVersion = null)
 * @method Dosya|null findOneBy(array $criteria, array $orderBy = null)
 * @method Dosya[]    findAll()
 * @method Dosya[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UploadFormRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Dosya::class);
    }

    // /**
    //  * @return Dosya[] Returns an array of Dosya objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UploadForm
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
