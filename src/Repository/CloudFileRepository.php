<?php

namespace App\Repository;

use App\Entity\CloudFile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CloudFile|null find($id, $lockMode = null, $lockVersion = null)
 * @method CloudFile|null findOneBy(array $criteria, array $orderBy = null)
 * @method CloudFile[]    findAll()
 * @method CloudFile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CloudFileRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CloudFile::class);
    }

    // /**
    //  * @return CloudFile[] Returns an array of CloudFile objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CloudFile
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
